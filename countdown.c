///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   Reference date 100 year in the future (the timer counts down because current time is going toward)
//   ./countdown
//
// @author Geoffrey Teocson <gteocson@hawaii.edu>
// @date   09/02/2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>          //struct tm
#include <unistd.h>        //sleep()

int main() {

   struct tm hardcoded_date = {0};        //Reference date
   hardcoded_date.tm_mday = 2;
   hardcoded_date.tm_mon = 3;
   hardcoded_date.tm_year = 200;
   time_t settime = mktime(&hardcoded_date);

   printf("\nReference time is:%sHST",(ctime(&settime)));
   

   while(1){
   
   
      time_t current;            //Simply refreshing the current time of the computer
   
      time_t time1,time2;
   
      //struct tm *currenttime;
   
      time ( &current );

   
   
      if(current<settime){          //Time in the future
   
      
         time1=settime;
      
         time2=current;
   
   
      }
   
      else{

      
         time1=current;             //Time in the past
      
         time2=settime;

   
      }

   
      unsigned int diff = (unsigned int)difftime(time1,time2); //Find the difference between 2 times as an unsigned integer
   
   
      int yearsub = diff / 31536000;         //Find how much year in between then subtract that amount of seconds

   
      diff = diff - ( yearsub * 31536000 );

   
      int daysub = diff / 86400;             //Find how much days in between and subtract that amount of seconds

   
      diff = diff - ( daysub * 86400);

   
      int hoursub = diff / 3600;             //Find how much hours in between and subtract that amount of seconds

   
      diff = diff - ( hoursub * 3600);

   
      int minsub = diff / 60;                //Find how much minutes in between and subtract that amount of seconds

   
      diff = diff - ( minsub * 60);          //Leftover diff should be amount of seconds
   

      printf("\nYears:%d Days:%d Hours:%d Mins:%d Seconds:%d",yearsub,daysub,hoursub,minsub,diff);
   
      sleep(1);         //Pause for 1 second
   
   }

}
